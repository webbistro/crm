import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import currencyFilter from '@/filters/currency.filter'
import messagePlugin from '@/utils/message.plugin'
import Loader from '@/components/app/Loader'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false;

Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.component('Loader', Loader)

firebase.initializeApp({
  apiKey: "AIzaSyBHTOctA1I3pie0-3XKkGXH2eRkChhK7vY",
  authDomain: "my-vue-crm-3a2b8.firebaseapp.com",
  databaseURL: "https://my-vue-crm-3a2b8.firebaseio.com",
  projectId: "my-vue-crm-3a2b8",
  storageBucket: "my-vue-crm-3a2b8.appspot.com",
  messagingSenderId: "706442493664",
  appId: "1:706442493664:web:69f14c6f2d479586425a85"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if(!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
